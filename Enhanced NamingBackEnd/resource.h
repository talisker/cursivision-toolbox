// Copyright 2017 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ToolBoxResources.h"

#define IDDI_NAME1_PREFIX      10001
#define IDDI_NAME2_PREFIX      10002
#define IDDI_NAME3_PREFIX      10003

#define IDD_NO_PROFILE        1

#define IDS_NO_PROFILE        2

#define IDDI_NO_PROFILE_NOTE  101
